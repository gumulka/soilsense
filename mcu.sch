EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+BATT #PWR?
U 1 1 61C4B2CB
P 3150 2000
AR Path="/61C4B2CB" Ref="#PWR?"  Part="1" 
AR Path="/61C210F3/61C4B2CB" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 3150 1850 50  0001 C CNN
F 1 "+BATT" H 3165 2173 50  0000 C CNN
F 2 "" H 3150 2000 50  0001 C CNN
F 3 "" H 3150 2000 50  0001 C CNN
	1    3150 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 61C4B5C2
P 3400 5900
F 0 "#PWR0105" H 3400 5650 50  0001 C CNN
F 1 "GND" H 3405 5727 50  0000 C CNN
F 2 "" H 3400 5900 50  0001 C CNN
F 3 "" H 3400 5900 50  0001 C CNN
	1    3400 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 5650 3400 5650
Connection ~ 3400 5650
Wire Wire Line
	3400 5650 3400 5900
Text Notes 2550 7500 0    50   ~ 0
Wakeup Capable Pins:\nPA0, PB3, and PC13
Text Notes 2600 7150 0    50   ~ 0
BOOT0 = 1 Means start bootloader\nPA9/PA10 or PA2/PA3 are Pins for loading Firmware over UART
Text HLabel 4000 3750 2    50   Input ~ 0
Button
Text HLabel 2350 4750 0    50   Input ~ 0
SOIL_SENS
$Comp
L Memory_EEPROM:M24C02-FMN U?
U 1 1 61C5EFFC
P 1350 6950
AR Path="/61C5EFFC" Ref="U?"  Part="1" 
AR Path="/61C210F3/61C5EFFC" Ref="U1"  Part="1" 
F 0 "U1" H 1350 7431 50  0000 C CNN
F 1 "M24C02-FMN" H 1350 7340 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 1350 7300 50  0001 C CNN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/b0/d8/50/40/5a/85/49/6f/DM00071904.pdf/files/DM00071904.pdf/jcr:content/translations/en.DM00071904.pdf" H 1400 6450 50  0001 C CNN
	1    1350 6950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61C5F002
P 1350 7350
AR Path="/61C5F002" Ref="#PWR?"  Part="1" 
AR Path="/61C210F3/61C5F002" Ref="#PWR0106"  Part="1" 
F 0 "#PWR0106" H 1350 7100 50  0001 C CNN
F 1 "GND" H 1355 7177 50  0000 C CNN
F 2 "" H 1350 7350 50  0001 C CNN
F 3 "" H 1350 7350 50  0001 C CNN
	1    1350 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  6850 900  6850
Wire Wire Line
	900  6850 900  6950
Wire Wire Line
	900  7300 1350 7300
Wire Wire Line
	1350 7300 1350 7250
Wire Wire Line
	1350 7300 1350 7350
Connection ~ 1350 7300
Wire Wire Line
	950  6950 900  6950
Connection ~ 900  6950
Wire Wire Line
	900  6950 900  7050
Wire Wire Line
	950  7050 900  7050
Connection ~ 900  7050
Wire Wire Line
	900  7050 900  7300
$Comp
L Device:R R?
U 1 1 61C5F014
P 2000 6650
AR Path="/61C5F014" Ref="R?"  Part="1" 
AR Path="/61C210F3/61C5F014" Ref="R7"  Part="1" 
F 0 "R7" H 2070 6696 50  0000 L CNN
F 1 "10K" H 2070 6605 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1930 6650 50  0001 C CNN
F 3 "~" H 2000 6650 50  0001 C CNN
	1    2000 6650
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 61C5F01A
P 2300 6650
AR Path="/61C5F01A" Ref="R?"  Part="1" 
AR Path="/61C210F3/61C5F01A" Ref="R8"  Part="1" 
F 0 "R8" H 2370 6696 50  0000 L CNN
F 1 "10K" H 2370 6605 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2230 6650 50  0001 C CNN
F 3 "~" H 2300 6650 50  0001 C CNN
	1    2300 6650
	-1   0    0    1   
$EndComp
Text GLabel 1350 6300 1    50   Input ~ 0
VCC_I2C
Wire Wire Line
	1350 6300 1350 6450
Wire Wire Line
	1350 6450 2000 6450
Connection ~ 1350 6450
Wire Wire Line
	1350 6450 1350 6650
Connection ~ 2000 6450
Wire Wire Line
	2000 6850 1750 6850
Wire Wire Line
	2300 6450 2300 6500
Wire Wire Line
	2000 6450 2300 6450
Wire Wire Line
	2300 6800 2300 6950
Wire Wire Line
	2300 6950 1750 6950
Wire Wire Line
	2000 6450 2000 6500
Wire Wire Line
	2000 6800 2000 6850
NoConn ~ 1750 7050
Text Label 1750 6850 0    50   ~ 0
SDA
Text Label 1750 6950 0    50   ~ 0
SCL
Text Label 2350 5050 0    50   ~ 0
SDA
Text Label 2350 4950 0    50   ~ 0
SCL
$Comp
L Connector:TestPoint TP5
U 1 1 61C7CB86
P 4150 3950
F 0 "TP5" V 4104 4138 50  0000 L CNN
F 1 "TX" V 4195 4138 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 4350 3950 50  0001 C CNN
F 3 "~" H 4350 3950 50  0001 C CNN
	1    4150 3950
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP6
U 1 1 61C7D456
P 4150 4050
F 0 "TP6" V 4104 4238 50  0000 L CNN
F 1 "RX" V 4195 4238 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 4350 4050 50  0001 C CNN
F 3 "~" H 4350 4050 50  0001 C CNN
	1    4150 4050
	0    1    1    0   
$EndComp
Text GLabel 2250 4850 0    50   Input ~ 0
VCC_I2C
Text HLabel 4000 4150 2    50   Input ~ 0
LED
Wire Wire Line
	3150 2000 3150 2100
Wire Wire Line
	3150 2100 3250 2100
Connection ~ 3150 2100
Wire Wire Line
	3250 2100 3350 2100
Connection ~ 3250 2100
Wire Wire Line
	3350 2100 3450 2100
Connection ~ 3350 2100
$Comp
L Device:Antenna AE?
U 1 1 61C9135A
P 10500 1000
AR Path="/61C9135A" Ref="AE?"  Part="1" 
AR Path="/61C210F3/61C9135A" Ref="AE1"  Part="1" 
F 0 "AE1" H 10580 989 50  0000 L CNN
F 1 "Antenna" H 10580 898 50  0000 L CNN
F 2 "RF_Antenna:Texas_SWRA416_868MHz_915MHz" H 10500 1000 50  0001 C CNN
F 3 "~" H 10500 1000 50  0001 C CNN
	1    10500 1000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C20
U 1 1 61C98CBB
P 10300 1550
F 0 "C20" H 10415 1596 50  0000 L CNN
F 1 "3.3pF" H 10415 1505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10338 1400 50  0001 C CNN
F 3 "~" H 10300 1550 50  0001 C CNN
	1    10300 1550
	1    0    0    -1  
$EndComp
$Comp
L Device:L L5
U 1 1 61C9969A
P 10000 1400
F 0 "L5" V 10190 1400 50  0000 C CNN
F 1 "8.7nH" V 10099 1400 50  0000 C CNN
F 2 "Inductor_SMD:L_0402_1005Metric" H 10000 1400 50  0001 C CNN
F 3 "~" H 10000 1400 50  0001 C CNN
	1    10000 1400
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C18
U 1 1 61C99C0B
P 9650 1550
F 0 "C18" H 9765 1596 50  0000 L CNN
F 1 "3.3pF" H 9765 1505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9688 1400 50  0001 C CNN
F 3 "~" H 9650 1550 50  0001 C CNN
	1    9650 1550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C17
U 1 1 61C9A0B8
P 9300 1400
F 0 "C17" V 9048 1400 50  0000 C CNN
F 1 "33pF" V 9139 1400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9338 1250 50  0001 C CNN
F 3 "~" H 9300 1400 50  0001 C CNN
	1    9300 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	10500 1200 10500 1400
Wire Wire Line
	10500 1400 10300 1400
Wire Wire Line
	10300 1400 10150 1400
Connection ~ 10300 1400
Wire Wire Line
	9850 1400 9650 1400
Wire Wire Line
	9450 1400 9650 1400
Connection ~ 9650 1400
$Comp
L Device:L L2
U 1 1 61CA4352
P 7450 3000
F 0 "L2" V 7640 3000 50  0000 C CNN
F 1 "3.3nH" V 7549 3000 50  0000 C CNN
F 2 "Inductor_SMD:L_0402_1005Metric" H 7450 3000 50  0001 C CNN
F 3 "~" H 7450 3000 50  0001 C CNN
	1    7450 3000
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L3
U 1 1 61CA60D2
P 7800 2750
F 0 "L3" H 7853 2796 50  0000 L CNN
F 1 "47nH" H 7853 2705 50  0000 L CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 7800 2750 50  0001 C CNN
F 3 "~" H 7800 2750 50  0001 C CNN
	1    7800 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 2600 7800 2300
Wire Wire Line
	7800 2900 7800 3000
Wire Wire Line
	7800 3000 7600 3000
Wire Wire Line
	7300 3000 6900 3000
Text Label 6900 3000 0    50   ~ 0
RFO_LP
Text Label 7800 2300 0    50   ~ 0
VDD_LP
Text Label 4200 3500 2    50   ~ 0
VDD_LP
Text Label 4200 2900 2    50   ~ 0
RFO_LP
$Comp
L Device:C_Small C13
U 1 1 61CAA3D7
P 7350 2450
F 0 "C13" H 7442 2496 50  0000 L CNN
F 1 "33pF" H 7442 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7350 2450 50  0001 C CNN
F 3 "~" H 7350 2450 50  0001 C CNN
	1    7350 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C14
U 1 1 61CAB2F8
P 8200 3100
F 0 "C14" H 8292 3146 50  0000 L CNN
F 1 "56pF" H 8292 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8200 3100 50  0001 C CNN
F 3 "~" H 8200 3100 50  0001 C CNN
	1    8200 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C16
U 1 1 61CAB68B
P 9100 3100
F 0 "C16" H 9192 3146 50  0000 L CNN
F 1 "3.3pF" H 9192 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9100 3100 50  0001 C CNN
F 3 "~" H 9100 3100 50  0001 C CNN
	1    9100 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C15
U 1 1 61CABA16
P 8700 2700
F 0 "C15" V 8929 2700 50  0000 C CNN
F 1 "2.7pF" V 8838 2700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8700 2700 50  0001 C CNN
F 3 "~" H 8700 2700 50  0001 C CNN
	1    8700 2700
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 61CAC4B5
P 7350 2650
F 0 "#PWR0107" H 7350 2400 50  0001 C CNN
F 1 "GND" H 7355 2477 50  0000 C CNN
F 2 "" H 7350 2650 50  0001 C CNN
F 3 "" H 7350 2650 50  0001 C CNN
	1    7350 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 2300 7350 2300
Wire Wire Line
	7350 2300 7350 2350
Wire Wire Line
	7350 2550 7350 2650
$Comp
L Device:C_Small C21
U 1 1 61CB75A8
P 10550 3100
F 0 "C21" H 10642 3146 50  0000 L CNN
F 1 "1.3pF" H 10642 3055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10550 3100 50  0001 C CNN
F 3 "~" H 10550 3100 50  0001 C CNN
	1    10550 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C19
U 1 1 61CB92CA
P 9650 3000
F 0 "C19" V 9879 3000 50  0000 C CNN
F 1 "33pF" V 9788 3000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9650 3000 50  0001 C CNN
F 3 "~" H 9650 3000 50  0001 C CNN
	1    9650 3000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9750 3000 10550 3000
Wire Wire Line
	9550 3000 9100 3000
$Comp
L Device:L L4
U 1 1 61CBB618
P 8700 3000
F 0 "L4" V 8890 3000 50  0000 C CNN
F 1 "3nH" V 8799 3000 50  0000 C CNN
F 2 "Inductor_SMD:L_0402_1005Metric" H 8700 3000 50  0001 C CNN
F 3 "~" H 8700 3000 50  0001 C CNN
	1    8700 3000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8850 3000 9100 3000
Connection ~ 9100 3000
Wire Wire Line
	8800 2700 9100 2700
Wire Wire Line
	9100 2700 9100 3000
Wire Wire Line
	8550 3000 8200 3000
Wire Wire Line
	8200 3000 7800 3000
Connection ~ 8200 3000
Connection ~ 7800 3000
Wire Wire Line
	8600 2700 8200 2700
Wire Wire Line
	8200 2700 8200 3000
$Comp
L power:GND #PWR0108
U 1 1 61CC0178
P 8200 3350
F 0 "#PWR0108" H 8200 3100 50  0001 C CNN
F 1 "GND" H 8205 3177 50  0000 C CNN
F 2 "" H 8200 3350 50  0001 C CNN
F 3 "" H 8200 3350 50  0001 C CNN
	1    8200 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 61CC04F0
P 9100 3350
F 0 "#PWR0109" H 9100 3100 50  0001 C CNN
F 1 "GND" H 9105 3177 50  0000 C CNN
F 2 "" H 9100 3350 50  0001 C CNN
F 3 "" H 9100 3350 50  0001 C CNN
	1    9100 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 61CC08F3
P 10550 3350
F 0 "#PWR0110" H 10550 3100 50  0001 C CNN
F 1 "GND" H 10555 3177 50  0000 C CNN
F 2 "" H 10550 3350 50  0001 C CNN
F 3 "" H 10550 3350 50  0001 C CNN
	1    10550 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	10550 3350 10550 3200
Wire Wire Line
	9100 3200 9100 3350
Wire Wire Line
	8200 3200 8200 3350
Text Label 11100 3000 2    50   ~ 0
RFO_L_Filtered
Wire Wire Line
	10550 3000 11100 3000
Connection ~ 10550 3000
$Comp
L Device:L L1
U 1 1 61CC65D3
P 4750 3350
F 0 "L1" H 4803 3396 50  0000 L CNN
F 1 "11nH" H 4803 3305 50  0000 L CNN
F 2 "Inductor_SMD:L_0402_1005Metric" H 4750 3350 50  0001 C CNN
F 3 "~" H 4750 3350 50  0001 C CNN
	1    4750 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C11
U 1 1 61CC6E2A
P 4750 3750
F 0 "C11" H 4842 3796 50  0000 L CNN
F 1 "2.7pF" H 4842 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4750 3750 50  0001 C CNN
F 3 "~" H 4750 3750 50  0001 C CNN
	1    4750 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C12
U 1 1 61CC7600
P 5200 3200
F 0 "C12" V 4971 3200 50  0000 C CNN
F 1 "3.6pF" V 5062 3200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5200 3200 50  0001 C CNN
F 3 "~" H 5200 3200 50  0001 C CNN
	1    5200 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	4750 3500 4750 3550
Wire Wire Line
	4550 3300 4550 3550
Wire Wire Line
	4550 3550 4750 3550
Connection ~ 4750 3550
Wire Wire Line
	4750 3550 4750 3650
Wire Wire Line
	5100 3200 4750 3200
Connection ~ 4750 3200
$Comp
L power:GND #PWR0111
U 1 1 61CF102D
P 4750 3950
F 0 "#PWR0111" H 4750 3700 50  0001 C CNN
F 1 "GND" H 4755 3777 50  0000 C CNN
F 2 "" H 4750 3950 50  0001 C CNN
F 3 "" H 4750 3950 50  0001 C CNN
	1    4750 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3950 4750 3850
Text Label 5850 3200 2    50   ~ 0
RFI_Filtered
Wire Wire Line
	5850 3200 5300 3200
Wire Wire Line
	2400 2750 2400 2050
$Comp
L Device:R R9
U 1 1 61CFF85A
P 2650 2050
F 0 "R9" V 2443 2050 50  0000 C CNN
F 1 "0R" V 2534 2050 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2580 2050 50  0001 C CNN
F 3 "~" H 2650 2050 50  0001 C CNN
	1    2650 2050
	0    1    1    0   
$EndComp
Wire Wire Line
	2850 2050 2800 2050
Wire Wire Line
	2500 2050 2400 2050
$Comp
L Device:R R10
U 1 1 61D054D8
P 2950 2050
F 0 "R10" H 2880 2004 50  0000 R CNN
F 1 "0R" H 2880 2095 50  0000 R CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2880 2050 50  0001 C CNN
F 3 "~" H 2950 2050 50  0001 C CNN
	1    2950 2050
	-1   0    0    1   
$EndComp
Wire Wire Line
	3150 2100 3050 2100
Wire Wire Line
	3050 2100 3050 1850
Wire Wire Line
	3050 1850 2950 1850
Wire Wire Line
	2950 1850 2950 1900
$Comp
L Connector:TestPoint TP1
U 1 1 61D0CACD
P 1350 3750
F 0 "TP1" H 1408 3868 50  0000 L CNN
F 1 "BOOT0" H 1408 3777 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 1550 3750 50  0001 C CNN
F 3 "~" H 1550 3750 50  0001 C CNN
	1    1350 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal Y1
U 1 1 61D14026
P 2000 4200
F 0 "Y1" V 1954 4331 50  0000 L CNN
F 1 "32.768kHz" V 2045 4331 50  0000 L CNN
F 2 "" H 2000 4200 50  0001 C CNN
F 3 "~" H 2000 4200 50  0001 C CNN
	1    2000 4200
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C3
U 1 1 61D18722
P 1600 4050
F 0 "C3" V 1371 4050 50  0000 C CNN
F 1 "5.1pF" V 1462 4050 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1600 4050 50  0001 C CNN
F 3 "~" H 1600 4050 50  0001 C CNN
	1    1600 4050
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C4
U 1 1 61D198B2
P 1600 4350
F 0 "C4" V 1371 4350 50  0000 C CNN
F 1 "5.1pF" V 1462 4350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1600 4350 50  0001 C CNN
F 3 "~" H 1600 4350 50  0001 C CNN
	1    1600 4350
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 4050 2000 4050
Wire Wire Line
	1700 4350 2000 4350
Wire Wire Line
	2000 4050 2400 4050
Wire Wire Line
	2400 4050 2400 4150
Connection ~ 2000 4050
Wire Wire Line
	2600 5050 2350 5050
Wire Wire Line
	3400 5450 3400 5650
Wire Wire Line
	3300 5450 3300 5650
Wire Wire Line
	3250 2100 3250 2250
Wire Wire Line
	3450 2100 3450 2250
Wire Wire Line
	2600 4950 2350 4950
Wire Wire Line
	2400 4150 2600 4150
Wire Wire Line
	3150 2100 3150 2250
Wire Wire Line
	4150 4050 3900 4050
Wire Wire Line
	2600 2750 2400 2750
Wire Wire Line
	2950 2250 2950 2200
Wire Wire Line
	2850 2050 2850 2250
Wire Wire Line
	3900 3500 4200 3500
Wire Wire Line
	4200 2900 3900 2900
Wire Wire Line
	3900 3300 4550 3300
Wire Wire Line
	3900 3200 4750 3200
Wire Wire Line
	2600 3750 2450 3750
Wire Wire Line
	4150 3950 3900 3950
Wire Wire Line
	3350 2100 3350 2250
$Comp
L MCU_STM32WL:STM32WLE5 U2
U 1 1 61C219BA
P 3300 3850
F 0 "U2" H 2800 2300 50  0000 C CNN
F 1 "STM32WLE5" H 3700 2250 50  0000 C CNN
F 2 "Package_DFN_QFN:QFN-48-1EP_7x7mm_P0.5mm_EP5.6x5.6mm" H 3450 5950 50  0000 C CNN
F 3 "https://www.st.com/resource/en/datasheet/stm32wle5jc.pdf" H 3300 3850 50  0001 C CNN
	1    3300 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 4350 2400 4350
Wire Wire Line
	2400 4350 2400 4250
Wire Wire Line
	2400 4250 2600 4250
Connection ~ 2000 4350
$Comp
L power:GND #PWR0112
U 1 1 61D29715
P 1400 4050
F 0 "#PWR0112" H 1400 3800 50  0001 C CNN
F 1 "GND" V 1405 3922 50  0000 R CNN
F 2 "" H 1400 4050 50  0001 C CNN
F 3 "" H 1400 4050 50  0001 C CNN
	1    1400 4050
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 61D2A308
P 1400 4350
F 0 "#PWR0113" H 1400 4100 50  0001 C CNN
F 1 "GND" V 1405 4222 50  0000 R CNN
F 2 "" H 1400 4350 50  0001 C CNN
F 3 "" H 1400 4350 50  0001 C CNN
	1    1400 4350
	0    1    1    0   
$EndComp
Wire Wire Line
	1400 4050 1500 4050
Wire Wire Line
	1500 4350 1400 4350
$Comp
L power:+BATT #PWR?
U 1 1 61D36170
P 3200 750
AR Path="/61D36170" Ref="#PWR?"  Part="1" 
AR Path="/61C210F3/61D36170" Ref="#PWR0114"  Part="1" 
F 0 "#PWR0114" H 3200 600 50  0001 C CNN
F 1 "+BATT" H 3215 923 50  0000 C CNN
F 2 "" H 3200 750 50  0001 C CNN
F 3 "" H 3200 750 50  0001 C CNN
	1    3200 750 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 61D3686E
P 3200 1150
F 0 "#PWR0115" H 3200 900 50  0001 C CNN
F 1 "GND" H 3205 977 50  0000 C CNN
F 2 "" H 3200 1150 50  0001 C CNN
F 3 "" H 3200 1150 50  0001 C CNN
	1    3200 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C7
U 1 1 61D37370
P 3200 950
F 0 "C7" H 3108 904 50  0000 R CNN
F 1 "100nF" H 3108 995 50  0000 R CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3200 950 50  0001 C CNN
F 3 "~" H 3200 950 50  0001 C CNN
	1    3200 950 
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C8
U 1 1 61D38BCD
P 3600 950
F 0 "C8" H 3508 904 50  0000 R CNN
F 1 "100nF" H 3508 995 50  0000 R CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3600 950 50  0001 C CNN
F 3 "~" H 3600 950 50  0001 C CNN
	1    3600 950 
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C9
U 1 1 61D39085
P 4000 950
F 0 "C9" H 3908 904 50  0000 R CNN
F 1 "100nF" H 3908 995 50  0000 R CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4000 950 50  0001 C CNN
F 3 "~" H 4000 950 50  0001 C CNN
	1    4000 950 
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C10
U 1 1 61D39589
P 4400 950
F 0 "C10" H 4308 904 50  0000 R CNN
F 1 "100nF" H 4308 995 50  0000 R CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4400 950 50  0001 C CNN
F 3 "~" H 4400 950 50  0001 C CNN
	1    4400 950 
	-1   0    0    1   
$EndComp
Wire Wire Line
	3200 750  3200 800 
Wire Wire Line
	3200 1150 3200 1100
Wire Wire Line
	3200 1100 3600 1100
Wire Wire Line
	3600 1100 3600 1050
Connection ~ 3200 1100
Wire Wire Line
	3200 1100 3200 1050
Wire Wire Line
	3600 1100 4000 1100
Wire Wire Line
	4000 1100 4000 1050
Connection ~ 3600 1100
Wire Wire Line
	4000 1100 4400 1100
Wire Wire Line
	4400 1100 4400 1050
Connection ~ 4000 1100
Wire Wire Line
	3600 850  3600 800 
Wire Wire Line
	3600 800  3200 800 
Connection ~ 3200 800 
Wire Wire Line
	3200 800  3200 850 
Wire Wire Line
	3600 800  4000 800 
Wire Wire Line
	4000 800  4000 850 
Connection ~ 3600 800 
Wire Wire Line
	4000 800  4400 800 
Wire Wire Line
	4400 800  4400 850 
Connection ~ 4000 800 
Wire Wire Line
	2600 4850 2250 4850
$Comp
L Connector:TestPoint TP2
U 1 1 61D5E917
P 1700 4650
F 0 "TP2" V 1895 4722 50  0000 C CNN
F 1 "SWO" V 1804 4722 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 1900 4650 50  0001 C CNN
F 3 "~" H 1900 4650 50  0001 C CNN
	1    1700 4650
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP7
U 1 1 61D5F0EE
P 4200 5050
F 0 "TP7" V 4154 5238 50  0000 L CNN
F 1 "SWDIO" V 4245 5238 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 4400 5050 50  0001 C CNN
F 3 "~" H 4400 5050 50  0001 C CNN
	1    4200 5050
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 61D60266
P 1750 3600
F 0 "TP3" V 1945 3672 50  0000 C CNN
F 1 "RST" V 1854 3672 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 1950 3600 50  0001 C CNN
F 3 "~" H 1950 3600 50  0001 C CNN
	1    1750 3600
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 61D60B38
P 4000 5150
F 0 "TP4" V 3954 5338 50  0000 L CNN
F 1 "SWCLK" V 4045 5338 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 4200 5150 50  0001 C CNN
F 3 "~" H 4200 5150 50  0001 C CNN
	1    4000 5150
	0    1    1    0   
$EndComp
Wire Wire Line
	4200 5050 3900 5050
Wire Wire Line
	4000 5150 3900 5150
Wire Wire Line
	1750 3600 2600 3600
Wire Wire Line
	2600 4650 1700 4650
Wire Wire Line
	2350 4750 2600 4750
Wire Wire Line
	4000 3750 3900 3750
Wire Wire Line
	4000 4150 3900 4150
Text HLabel 4150 3850 2    50   Input ~ 0
PWM_OUT
Wire Wire Line
	4150 3850 3900 3850
Text HLabel 4050 4950 2    50   Input ~ 0
NTC1
Text HLabel 4050 4850 2    50   Input ~ 0
NTC2
Text HLabel 2450 4550 0    50   Input ~ 0
VDD_NTC
Wire Wire Line
	4050 4950 3900 4950
Wire Wire Line
	4050 4850 3900 4850
Wire Wire Line
	2450 4550 2600 4550
NoConn ~ 2600 3300
NoConn ~ 2600 3400
NoConn ~ 3900 3000
Wire Wire Line
	2600 4050 2450 4050
Wire Wire Line
	2450 4050 2450 3750
Connection ~ 2450 3750
$Comp
L Device:R R6
U 1 1 61E4C4B2
P 1000 3750
F 0 "R6" V 793 3750 50  0000 C CNN
F 1 "100k" V 884 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 930 3750 50  0001 C CNN
F 3 "~" H 1000 3750 50  0001 C CNN
	1    1000 3750
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 61E5905E
P 750 3750
F 0 "#PWR0116" H 750 3500 50  0001 C CNN
F 1 "GND" V 755 3622 50  0000 R CNN
F 2 "" H 750 3750 50  0001 C CNN
F 3 "" H 750 3750 50  0001 C CNN
	1    750  3750
	0    1    1    0   
$EndComp
Wire Wire Line
	850  3750 750  3750
Wire Wire Line
	1150 3750 1350 3750
Wire Wire Line
	1350 3750 2450 3750
Connection ~ 1350 3750
$EndSCHEMATC
